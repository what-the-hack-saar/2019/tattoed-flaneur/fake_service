============
Fake service
============


.. image:: https://img.shields.io/pypi/v/fake_service.svg
        :target: https://pypi.python.org/pypi/fake_service

.. image:: https://img.shields.io/travis/audreyr/fake_service.svg
        :target: https://travis-ci.org/audreyr/fake_service

.. image:: https://readthedocs.org/projects/fake-service/badge/?version=latest
        :target: https://fake-service.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: MIT license
* Documentation: https://fake-service.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
